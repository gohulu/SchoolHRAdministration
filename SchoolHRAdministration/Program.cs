﻿using System;
using System.Collections.Generic;
using HRAdministrationAPI;
using System.Linq;

namespace SchoolHRAdministration
{
    public enum EmployeeType
    {
        Teacher,
        HeadOfDepartment,
        DeputyHeadMaster,
        HeadMaster
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<IEmployee> employees = new List<IEmployee>();
            SeedData(employees);

            Console.WriteLine($"Total Annual Salaries (including bonus): {employees.Sum(e => e.Salary)}");
            Console.ReadKey();
        }

        public static void SeedData(List<IEmployee> employees)
        {
            IEmployee teacher1 = EmployeeFactory.GetEmployeeInstance(EmployeeType.Teacher, 1, "Peter", "Parker", 40000);
            employees.Add(teacher1);

            IEmployee teacher2 = EmployeeFactory.GetEmployeeInstance(EmployeeType.Teacher, 2, "Mary", "Poppins", 50000);
            employees.Add(teacher2);
            
            IEmployee headOfDepartment = EmployeeFactory.GetEmployeeInstance(EmployeeType.HeadOfDepartment, 3, "Charlie", "Sheen", 60000);
            employees.Add(headOfDepartment);

            IEmployee deputyHeadMaster = EmployeeFactory.GetEmployeeInstance(EmployeeType.DeputyHeadMaster, 4, "Ursula", "Peterson", 45000);
            employees.Add(deputyHeadMaster);
            
            IEmployee headMaster = EmployeeFactory.GetEmployeeInstance(EmployeeType.HeadMaster, 4, "Tyler", "Dydleson", 150000);
            employees.Add(headMaster);
        }
    }

    public class Teacher : EmployeeBase
    {
        public override decimal Salary { get => base.Salary + (base.Salary * 0.02m);  }
    }

    public class HeadOfDepartment : EmployeeBase
    {
        public override decimal Salary { get => base.Salary + (base.Salary * 0.03m); }
    }

    public class DeputyHeadMaster : EmployeeBase
    {
        public override decimal Salary { get => base.Salary + (base.Salary * 0.04m); }
    }

    public class HeadMaster : EmployeeBase
    {
        public override decimal Salary { get => base.Salary + (base.Salary * 0.05m); }
    }

    public static class EmployeeFactory
    {
        public static IEmployee GetEmployeeInstance(EmployeeType employeeType, int id, string firstName, string lastName, decimal salary)
        {
            IEmployee employee = GetEmployeeInstance(employeeType);

            if (employee == null)
            {
                throw new InvalidOperationException();
            }

            employee.Id = id;
            employee.FirstName = firstName;
            employee.LastName = lastName;
            employee.Salary = salary;

            return employee;
        }

        private static IEmployee GetEmployeeInstance(EmployeeType employeeType)
        {
            switch (employeeType)
            {
                case EmployeeType.Teacher:
                    return FactoryPattern<IEmployee, Teacher>.GetInstance();
                case EmployeeType.DeputyHeadMaster:
                    return FactoryPattern<IEmployee, DeputyHeadMaster>.GetInstance();
                case EmployeeType.HeadOfDepartment:
                    return FactoryPattern<IEmployee, HeadOfDepartment>.GetInstance();
                case EmployeeType.HeadMaster:
                    return FactoryPattern<IEmployee, HeadMaster>.GetInstance();
                default:
                    return null;
            }
        }
    }
}
